// SPDX-FileCopyrightText: 2019 Antonino Catinello
// SPDX-License-Identifier: BSD-3-Clause

package main

import (
	"crypto/tls"
	_ "embed"
	"errors"
	"fmt"
	"html/template"
	"log"
	"net"
	"net/http"
	"os"
	"strings"
	"time"

	"catinello.eu/com"
	"catinello.eu/lecot"
	"catinello.eu/restrict"
	"golang.org/x/crypto/acme/autocert"
)

var version string

//go:embed LICENSE
var license string

var (
	// disallow all bots everywhere
	robots string = `User-agent: *
Disallow: /`
)

type Addr struct {
	v4  string
	v6  string
	dns string
}

type Client struct {
	Name       string
	Header     bool
	Headers    []Header
	Footer     bool
	Disclaimer template.HTML
}

type Header struct {
	Key   string
	Value string
}

func initJS(dns string, w http.ResponseWriter, r *http.Request, logerr *log.Logger) {
	if r.Method != http.MethodGet {
		http.Error(w, http.StatusText(http.StatusMethodNotAllowed), http.StatusMethodNotAllowed)
		logerr.Println("MethodNotAllowed")
		return
	}

	w.Header().Set("Content-Type", "text/javascript; charset=utf-8")
	w.Header().Set("Content-Security-Policy", "upgrade-insecure-requests; default-src 'none';")
	w.Header().Set("Referrer-Policy", "no-referrer, strict-origin-when-cross-origin")
	w.Header().Set("X-Content-Type-Options", "nosniff")
	w.Header().Set("X-Frame-Options", "DENY")
	w.Header().Set("X-Robots-Tag", "noindex, nofollow")

	t, err := template.New("run.js").Parse(runJS)
	if err != nil {
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		logerr.Println(err)
		return
	}

	err = t.Execute(w, dns)
	if err != nil {
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		logerr.Println(err)
		return
	}

	return
}

func staticHeaders(dns string, fs http.Handler) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("X-Robots-Tag", "noindex, nofollow")
		w.Header().Set("Strict-Transport-Security", "max-age=63072000; includeSubDomains")
		w.Header().Set("Referrer-Policy", "no-referrer, strict-origin-when-cross-origin")
		w.Header().Set("X-Content-Type-Options", "nosniff")
		w.Header().Set("X-Frame-Options", "DENY")
		w.Header().Set("Permissions-Policy", "accelerometer=(), ambient-light-sensor=(), autoplay=(), battery=(), camera=(), cross-origin-isolated=(), display-capture=(), document-domain=(), encrypted-media=(), execution-while-not-rendered=(), execution-while-out-of-viewport=(), fullscreen=(), geolocation=(), gyroscope=(), keyboard-map=(), magnetometer=(), microphone=(), midi=(), navigation-override=(), payment=(), picture-in-picture=(), publickey-credentials-get=(), screen-wake-lock=(), sync-xhr=(), usb=(), web-share=(), xr-spatial-tracking=(), clipboard-read=(), clipboard-write=(), gamepad=(), speaker-selection=(), conversion-measurement=(), focus-without-user-activation=(), hid=(), idle-detection=(), interest-cohort=(), serial=(), sync-script=(), trust-token-redemption=(), window-placement=(), vertical-scroll=()")
		w.Header().Set("Access-Control-Allow-Origin", "https://"+dns)
		w.Header().Set("Access-Control-Allow-Methods", "GET")
		w.Header().Set("Content-Security-Policy", "upgrade-insecure-requests; default-src https://"+dns+"; frame-ancestors 'none'")
		fs.ServeHTTP(w, r)
	}
}

func getIPAddress(dns string, w http.ResponseWriter, r *http.Request, logerr *log.Logger, headers bool, footer string) {
	if r.Method != http.MethodGet {
		http.Error(w, http.StatusText(http.StatusMethodNotAllowed), http.StatusMethodNotAllowed)
		logerr.Println("MethodNotAllowed", r.RemoteAddr, r.UserAgent())
		return
	}

	if strings.Contains(r.UserAgent(), "bot") || strings.Contains(r.UserAgent(), "Bot") || strings.Contains(r.UserAgent(), "b0t") {
		http.Error(w, http.StatusText(http.StatusNotAcceptable), http.StatusNotAcceptable)
		logerr.Println("BotsNotAllowed", r.RemoteAddr, r.UserAgent())
		return
	}

	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	// Only connect to this site and subdomains via HTTPS for the next two years and also include in the preload list
	// https://infosec.mozilla.org/guidelines/web_security#http-strict-transport-security
	w.Header().Set("Strict-Transport-Security", "max-age=63072000; includeSubDomains")
	// Disable referrers for browsers that don't support strict-origin-when-cross-origin uses strict-origin-when-cross-origin for browsers that do
	// https://infosec.mozilla.org/guidelines/web_security#referrer-policy
	w.Header().Set("Referrer-Policy", "no-referrer, strict-origin-when-cross-origin")
	// Prevent browsers from incorrectly detecting non-scripts as scripts
	// https://infosec.mozilla.org/guidelines/web_security#x-content-type-options
	w.Header().Set("X-Content-Type-Options", "nosniff")
	// Block site from being framed with X-Frame-Options and CSP
	// https://infosec.mozilla.org/guidelines/web_security#x-frame-options
	w.Header().Set("X-Frame-Options", "DENY")
	// https://www.w3.org/TR/permissions-policy-1/
	w.Header().Set("Permissions-Policy", "accelerometer=(), ambient-light-sensor=(), autoplay=(), battery=(), camera=(), cross-origin-isolated=(), display-capture=(), document-domain=(), encrypted-media=(), execution-while-not-rendered=(), execution-while-out-of-viewport=(), fullscreen=(), geolocation=(), gyroscope=(), keyboard-map=(), magnetometer=(), microphone=(), midi=(), navigation-override=(), payment=(), picture-in-picture=(), publickey-credentials-get=(), screen-wake-lock=(), sync-xhr=(), usb=(), web-share=(), xr-spatial-tracking=(), clipboard-read=(), clipboard-write=(), gamepad=(), speaker-selection=(), conversion-measurement=(), focus-without-user-activation=(), hid=(), idle-detection=(), interest-cohort=(), serial=(), sync-script=(), trust-token-redemption=(), window-placement=(), vertical-scroll=()")
	w.Header().Set("X-Robots-Tag", "noindex, nofollow")

	// https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Access-Control-Allow-Origin
	if r.Host == "v6."+dns {
		w.Header().Set("Access-Control-Allow-Origin", "https://"+dns)
		w.Header().Set("Access-Control-Allow-Methods", "GET")
		w.Header().Set("Content-Security-Policy", "upgrade-insecure-requests; default-src 'none';")
		if headers {
			for k, v := range r.Header {
				fmt.Fprintln(w, k, v)
			}
		}
		fmt.Fprintf(w, "%s", strings.Trim(strings.TrimRight(r.RemoteAddr, ":0123456789"), "[]"))
	} else if r.Host == "v4."+dns {
		w.Header().Set("Access-Control-Allow-Origin", "https://"+dns)
		w.Header().Set("Access-Control-Allow-Methods", "GET")
		w.Header().Set("Content-Security-Policy", "upgrade-insecure-requests; default-src 'none';")
		if headers {
			for k, v := range r.Header {
				fmt.Fprintln(w, k, v)
			}
		}
		fmt.Fprintf(w, "%s", strings.Split(r.RemoteAddr, ":")[0])
	} else {
		// Disable the loading of any resources and disable framing
		// https://infosec.mozilla.org/guidelines/web_security#content-security-policy
		w.Header().Set("Content-Security-Policy", "upgrade-insecure-requests; default-src 'self' https://"+dns+" https://v4."+dns+" https://v6."+dns+"; frame-ancestors 'none'; worker-src 'none'; script-src https://"+dns+"/static/js/showAddress.js https://"+dns+"/js/run.js; object-src 'none'; style-src https://"+dns+"/static/css/style.css; font-src 'self'; img-src 'self'; connect-src 'self' https://v4."+dns+" https://v6."+dns+"; media-src 'none'; base-uri 'none'; child-src 'none'; frame-src 'none'; form-action 'none';")

		var ip string
		sc := strings.Count(r.RemoteAddr, ":")
		if sc > 1 {
			ip = strings.Trim(strings.TrimRight(r.RemoteAddr, ":0123456789"), "[]")
		} else {
			ip = strings.Split(r.RemoteAddr, ":")[0]
		}

		if !strings.HasPrefix(r.UserAgent(), "Mozilla/5.0 ") {
			if headers {
				for k, v := range r.Header {
					fmt.Fprintln(w, k, v)
				}
			}
			fmt.Fprintf(w, "%s\n", ip)
		} else {
			var c Client
			c.Name = dns
			c.Header = headers

			if len(footer) > 0 {
				c.Footer = true
				c.Disclaimer = template.HTML(footer)
			}

			if headers {
				for k, v := range r.Header {
					c.Headers = append(c.Headers, Header{Key: k, Value: strings.Join(v, " | ")})
				}
			}

			html(w, "default", c, logerr)
		}
	}

	return
}

func trim(i string) string {
	return strings.Replace(strings.TrimSpace(i), "\n", "", -1)
}

func html(w http.ResponseWriter, site string, data interface{}, logerr *log.Logger) {
	t, err := template.New(site).Parse(indexHTML)
	if err != nil {
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		logerr.Println(err)
		return
	}

	err = t.Execute(w, &data)
	if err != nil {
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		logerr.Println(err)
		return
	}

	return
}

func logging(handler http.Handler, logger *log.Logger) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		remoteIP, _, _ := net.SplitHostPort(r.RemoteAddr)
		logger.Printf("%s|https://%s%s|%s|%s\n", remoteIP, r.Host, r.URL.Path, r.Method, r.UserAgent())
		handler.ServeHTTP(w, r)
	})
}

func main() {
	var m *com.Config

	if len(os.Getenv("DEBUG")) > 0 {
		m = com.New(com.Debug)
		restrict.Debug = true
		restrict.Logger = m.Logger(com.Verbose, "restrict: ", false)
	} else {
		m = com.New(com.Common)
	}

	var a Addr
	var footerFile string
	var ft []byte
	var ns string

	m.D().Println(os.Args)
	// handle version and help calls
	if len(os.Args) > 1 {
		for i, v := range os.Args[1:] {
			m.D().Println(i, v)
			switch v {
			case "--version":
				m.Println(version)
				os.Exit(0)
			case "--license":
				m.Print(license)
				os.Exit(0)
			case "--help":
				help()
				os.Exit(0)
			case "--disable-restrictions":
				restrict.Disable()
			case "--verbose", "-v":
				if m.LevelIs() != com.Debug {
					m = com.New(com.Verbose)
				} else {
					m.E().Println("Verbose is not available in debug mode.")
					os.Exit(2)
				}
			case "--nameserver", "-n":
				if len(os.Args) > i+2 {
					ns = os.Args[i+2]
				} else {
					m.E().Println("Syntax error.")
					os.Exit(3)
				}
			case "--domain", "-d":
				if len(os.Args) > i+2 {
					a.dns = os.Args[i+2]
				} else {
					m.E().Println("Syntax error.")
					os.Exit(3)
				}
			case "--footer", "-f":
				if len(os.Args) > i+2 {
					footerFile = os.Args[i+2]
				} else {
					m.E().Println("Syntax error.")
					os.Exit(3)
				}
			}
		}
	}

	if err := restrict.Syscalls("stdio dns inet cpath rpath wpath landlock"); err != nil {
		m.E().Println(err)
		os.Exit(255)
	}

	restrict.Trust(lecot.Pool())
	if len(ns) > 0 {
		if err := restrict.Resolver(ns); err != nil {
			m.E().Println(err)
			os.Exit(255)
		}
	} else {
		if err := restrict.Resolver("9.9.9.9"); err != nil {
			m.E().Println(err)
			os.Exit(255)
		}
	}

	m.D().Println(a)
	m.D().Println(footerFile)

	if len(footerFile) > 0 {
		if err := restrict.Access(footerFile, "r"); err != nil {
			m.E().Println(err)
			os.Exit(255)
		}
	}

	pwd, err := os.Getwd()
	if err != nil {
		m.E().Println(err)
		os.Exit(1)
	}

	cache := pwd + string(os.PathSeparator) + ".certs"
	if _, err := os.Stat(cache); os.IsNotExist(err) {
		err := os.Mkdir(cache, 0700)
		if err != nil {
			m.E().Println(err)
			os.Exit(1)
		}
	}

	if err := restrict.Access(".certs", "crw"); err != nil {
		m.E().Println(err)
		os.Exit(255)
	}

	if err := restrict.AccessLock(); err != nil {
		m.E().Println(err)
		os.Exit(255)
	}

	if len(a.dns) == 0 {
		m.E().Println("You have to set a domainname!")
		os.Exit(1)
	}

	err = a.check()
	if err != nil {
		m.E().Println(err)
		os.Exit(2)
	}

	m.TimestampEnable("")
	logger := m.Logger(com.Verbose, "", false)
	logerr := m.Logger(com.Common, "", true)

	if len(footerFile) > 0 {
		var err error
		if ft, err = os.ReadFile(footerFile); err != nil {
			m.E().Println(err)
			os.Exit(1)
		}
	}

	if os.Geteuid() == 0 {
		m.E().Println("Not allowed to run as root!")
		os.Exit(1)
	}

	err = a.runServer(cache, logger, logerr, string(ft))
	if err != nil {
		m.E().Println(err)
		os.Exit(2)
	}

}

func (a *Addr) check() error {
	ip, err := net.LookupIP(a.dns)
	if err != nil {
		return err
	}

	for _, v := range ip {
		if strings.Contains(v.String(), ":") == true && len(a.v6) == 0 {
			a.v6 = v.String()
		}
		if strings.Contains(v.String(), ".") == true && len(a.v4) == 0 {
			a.v4 = v.String()
		}
	}

	if len(a.v4) == 0 {
		return errors.New("Missing IPv4 address.")
	}

	if len(a.v6) == 0 {
		return errors.New("Missing IPv4 address.")
	}

	v4, err := net.LookupIP("v4." + a.dns)
	if err != nil {
		return err
	}

	if v4[0].String() != a.v4 {
		return errors.New("Wrong IPv4 used on v4." + a.dns)
	}

	v6, err := net.LookupIP("v6." + a.dns)
	if err != nil {
		return err
	}

	if v6[0].String() != a.v6 {
		return errors.New("Wrong IPv6 used on v6." + a.dns)
	}

	return nil
}

func (a *Addr) runServer(cache string, logger *log.Logger, errlog *log.Logger, footer string) error {
	http.Handle("/static/", staticHeaders(a.dns, http.StripPrefix("/", http.FileServer(http.FS(static)))))

	http.HandleFunc("/js/run.js", func(w http.ResponseWriter, r *http.Request) {
		initJS(a.dns, w, r, errlog)
	})

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		getIPAddress(a.dns, w, r, errlog, false, footer)
	})

	http.HandleFunc("/headers", func(w http.ResponseWriter, r *http.Request) {
		getIPAddress(a.dns, w, r, errlog, true, footer)
	})

	http.HandleFunc("/LICENSE", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte(license))
	})

	http.HandleFunc("/VERSION", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte(version))
	})

	http.HandleFunc("/robots.txt", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte(robots))
	})

	m := &autocert.Manager{
		Cache:      autocert.DirCache(cache),
		Prompt:     autocert.AcceptTOS,
		HostPolicy: autocert.HostWhitelist("v6."+a.dns, "v4."+a.dns, a.dns),
	}

	go http.ListenAndServe(a.v4+":http", m.HTTPHandler(nil))
	go http.ListenAndServe("["+a.v6+"]"+":http", m.HTTPHandler(nil))

	t := &tls.Config{
		MinVersion:               tls.VersionTLS13,
		PreferServerCipherSuites: true,
		GetCertificate:           m.GetCertificate,
	}

	s4 := &http.Server{
		Addr:         a.v4 + ":https",
		ErrorLog:     errlog,
		Handler:      logging(http.DefaultServeMux, logger),
		TLSConfig:    t,
		ReadTimeout:  5 * time.Second,
		WriteTimeout: 10 * time.Second,
		IdleTimeout:  15 * time.Second,
	}

	go func(l *log.Logger) {
		err := s4.ListenAndServeTLS("", "")
		if err != nil {
			l.Println(err.Error())
			os.Exit(2)
		}
	}(errlog)

	s6 := &http.Server{
		Addr:         "[" + a.v6 + "]" + ":https",
		ErrorLog:     errlog,
		Handler:      logging(http.DefaultServeMux, logger),
		TLSConfig:    t,
		ReadTimeout:  5 * time.Second,
		WriteTimeout: 10 * time.Second,
		IdleTimeout:  15 * time.Second,
	}

	err := s6.ListenAndServeTLS("", "")
	if err != nil {
		return err
	}

	return nil
}

module myip

go 1.18

require (
	catinello.eu/com v0.0.0-20230917182302-140d3bd753ae
	catinello.eu/lecot v0.0.0-20220714203617-e4ce7f1a0185
	catinello.eu/restrict v0.0.0-20230916094435-88b3fa274bea
	golang.org/x/crypto v0.13.0
)

require (
	github.com/seccomp/libseccomp-golang v0.10.0 // indirect
	golang.org/x/net v0.15.0 // indirect
	golang.org/x/sys v0.12.0 // indirect
	golang.org/x/text v0.13.0 // indirect
	kernel.org/pub/linux/libs/security/libcap/psx v1.2.69 // indirect
)

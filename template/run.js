// SPDX-License-Identifier: BSD-3-Clause
check("v6", "{{.}}"); check("v4", "{{.}}");

document.getElementById("headersButton").onclick = function() {showHeaders()};

function showHeaders() {
    var t = document.getElementById("headersTable");
    t.style.display = (t.style.display == "table") ? "none" : "table";
}

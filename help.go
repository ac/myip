// SPDX-FileCopyrightText: 2019 Antonino Catinello
// SPDX-License-Identifier: BSD-3-Clause

package main

func help() {
	println("myip - Webservice to reply your clients IPv4/v6 addresses.")
	println()
	println("Usage:")
	println("  myip [OPTIONS]")
	println()
	println("Options:")
	println("  -d | --domain <DOMAIN.TLD>  | Use this name as base service name. (see Preparations)")
	println("  -f | --footer <FILE>        | Append this files content as footer.")
	println("  -n | --nameserver <IP>      | Nameserver to use. (default 9.9.9.9)")
	println("  -v | --verbose              | Print additional info and http logs.")
	println()
	println("  --help                      | Show this help.")
	println("  --license                   | Print license.")
	println("  --version                   | Print version.")
	println()
	println("Requirements:")
	println("  Dual stack system with publicly routable IP addresses for IPv4 and IPv6.")
	println()
	println("Preparations:")
	println("  <DOMAIN.TLD> needs 2 subdomains:")
	println("   → v4.DOMAIN.TLD and v6.DOMAIN.TLD with only the respective IP type assigned.")
	println("   → DOMAIN.TLD gets both records (A and AAAA) set.")
	println()
	println("Handles:")
	println("  /		(fallback) | Replies with IP(s) / Output depends on client (cli/browser).")
	println("  /headers		   | Replies with IP and received client headers.")
	println()
	println("Server Example:")
	println("  myip ip.example.com")
	println("   → Checks for DNS entries v4./v6.ip.example.com at runtime and fails if not correctly resolvable.")
	println()
	println("Client Examples:")
	println("  curl -4 https://ip.example.com # Replies only IPv4 because of curl's argument.")
	println("  curl -6 https://ip.example.com # Replies only IPv6 because of curl's argument.")
	println("  curl https://v4.ip.example.com # Replies only IPv4 because of the dns entry called.")
	println("  curl https://v6.ip.example.com # Replies only IPv6 because of the dns entry called.")
	println()
	println("Source:")
	println("  https://codeberg.org/ac/myip")
	println()
	println("License:")
	println("  BSD-3-Clause License © 2019 Antonino Catinello")
	println()
	println("Version:")
	println("  " + version)
}

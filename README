myip - Webservice to reply your clients IPv4/v6 addresses.

Usage:
  myip [OPTIONS]

Options:
  -d | --domain <DOMAIN.TLD>  | Use this name as base service name. (see Preparations)
  -f | --footer <FILE>        | Append this files content as footer.
  -n | --nameserver <IP>      | Nameserver to use. (default 9.9.9.9)
  -v | --verbose              | Print additional info and http logs.

  --help                      | Show this help.
  --license                   | Print license.
  --version                   | Print version.

Requirements:
  Dual stack system with publicly routable IP addresses for IPv4 and IPv6.

Preparations:
  <DOMAIN.TLD> needs 2 subdomains:
   → v4.DOMAIN.TLD and v6.DOMAIN.TLD with only the respective IP type assigned.
   → DOMAIN.TLD gets both records (A and AAAA) set.

Handles:
  /		(fallback) | Replies with IP(s) / Output depends on client (cli/browser).
  /headers		   | Replies with IP and received client headers.

Server Example:
  myip ip.example.com
   → Checks for DNS entries v4./v6.ip.example.com at runtime and fails if not correctly resolvable.

Client Examples:
  curl -4 https://ip.example.com # Replies only IPv4 because of curl's argument.
  curl -6 https://ip.example.com # Replies only IPv6 because of curl's argument.
  curl https://v4.ip.example.com # Replies only IPv4 because of the dns entry called.
  curl https://v6.ip.example.com # Replies only IPv6 because of the dns entry called.

Source:
  https://codeberg.org/ac/myip

License:
  BSD-3-Clause License © 2019 Antonino Catinello

// SPDX-License-Identifier: BSD-3-Clause

package main

import "embed"

//go:embed template/index.html
var indexHTML string

//go:embed template/run.js
var runJS string

// static web server content like css/js/fonts/icons.
//
//go:embed static/*
var static embed.FS

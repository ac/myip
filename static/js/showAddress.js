// SPDX-License-Identifier: BSD-3-Clause
function check(version, base) {
  var req = new XMLHttpRequest();
  req.open('GET', "https://" + version + "." + base);
  req.send();
  req.addEventListener('load', function(event) {
    if (req.readyState == 4 && req.status == 200) {
      console.log(req.statusText, req.responseText);
      //var response = JSON.parse(req.responseText);
      //document.write(response.ip);
      document.getElementById(version).innerHTML = "<p class=\"works\"><bold>IP" + version + ":</bold> <br/>" + req.responseText + "</p>";
    } else {
      console.warn(req.statusText, req.responseText);
      document.getElementById(version).innerHTML = "<p class=\"fails\"><bold>IP" + version + ":</bold> n/a</p>";
    }
  });
  req.addEventListener('error', function(event) {
    console.warn(req.statusText, req.responseText);
    document.getElementById(version).innerHTML = "<p class=\"fails\"><bold>IP" + version + ":</bold> n/a</p>";
  });
}


